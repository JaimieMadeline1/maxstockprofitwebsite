import { sum, consoleLog } from '../../common/helpers';

// const sum = (a, b) => {
//     return a + b;
// }

describe('firstTestBatch2', () => {
    var startDate;

    beforeAll(() => {
        consoleLog("Start");
        let currDate = new Date();
        startDate = currDate.getTime();
    })
    // beforeEach(() => {
    //     //do something
    // })
    afterAll(() => {
        consoleLog("End, diff: " + (new Date().getTime() - startDate));
    })
    it('adds 1 + 2 to equal 32', () => {
        consoleLog("Before Test, diff: " + (new Date().getTime() - startDate));
        expect(sum(1, 2)).toBe(3);
        consoleLog("Right AFter Test, diff: " + (new Date().getTime() - startDate));
    })
    describe('SecondBatch Check Hierarchy output2', () => {
        it('adds 7 + 1 to equal 82', () => {
            consoleLog("Before Test, diff: " + (new Date().getTime() - startDate));
            expect(sum(7, 1)).toBe(8);
            consoleLog("Right AFter Test, diff: " + (new Date().getTime() - startDate));
        })
    });
});
