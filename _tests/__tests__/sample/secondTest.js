// import { sum } from '../common/helpers';

const sum = (a, b) => {
  return a + b;
};

describe("secondTest", () => {
  it("adds 3 + 2 to equal 5", async () => {
    expect(sum(3, 2)).toBe(5);
  });
});
