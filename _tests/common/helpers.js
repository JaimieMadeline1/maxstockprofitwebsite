const sum = (a, b) => {
    return a + b;
}

const consoleLog = (str) => {
    // Makes it easier to comment out all console.log's with one comment
    // console.log(str);
}

export {
    sum,
    consoleLog
};