import React from 'react';

import styles from './AutoComplete.styl';

class SelectOptions extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: this.props.selectedIndex,
            item: this.props.data
        };
    }
    componentWillReceiveProps(props) {
        this.setState({
            selectedIndex: props.selectedIndex,
            item: props.data
        });
    }
    render() {
        return (
            <React.Fragment>
                {/* <div>{this.props.data.symbol}</div> */}
                {/* // custom class depending if it matches index */}
                <div key={this.state.item.index}
                    onMouseDown={() => this.props.preventBlur()} onMouseUp={() => this.props.allowBlur()}
                    onClick={() => this.props.onClickEvent(this.state.item.index)}
                    className={[styles.autoCompleteText,
                    `${this.state.selectedIndex == this.state.item.index ?
                        styles.autoCompleteTextSelected : styles.autoCompleteText}`].join(' ')}>
                    {this.state.item.index == 0 ?
                        <div>{this.state.item.symbol}</div>
                        :
                        <div>{this.state.item.symbol} - {this.state.item.name}</div>
                    }
                </div>
                {/* <div className="col-lg-3"></div> */}
            </React.Fragment>
        );
    }
}

export default SelectOptions;
