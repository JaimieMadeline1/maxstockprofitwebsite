import React from 'react';

import SelectOptions from './SelectOptions';
import styles from './AutoComplete.styl';

class SelectOptionsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIndex: this.props.selectedIndex,
            isShow: this.props.isShow,
            searchSuggestions: this.props.searchSuggestions
        };
    }
    componentWillReceiveProps(props) {
        this.setState({
            selectedIndex: props.selectedIndex,
            isShow: props.isShow,
            searchSuggestions: props.searchSuggestions
        });
    }
    render() {
        return (
            <React.Fragment>
                {/* http://jsfiddle.net/5uf2Y/1/ */}
                <div style={this.state.isShow ?
                    {} : { display: 'none' }}>
                    {/* { && */}
                    <div className={styles.searchSuggestionContainer}>
                        {this.state.searchSuggestions.map((item) =>
                            // custom class depending if it matches index
                            <SelectOptions key={item.index} data={item}
                                selectedIndex={this.state.selectedIndex}
                                preventBlur={() => this.props.preventBlur()} allowBlur={() => this.props.allowBlur()}
                                onClickEvent={(index) => this.props.onClickEvent(index)}>
                            </SelectOptions>
                            // <div key={item.index} onClick={this.userClickedSymbolSuggestion.bind(this, item.index)}
                            //     className={[styles.autoCompleteText, `${this.state.searchSuggestionSelectedIndex == item.index ?
                            //         styles.autoCompleteTextSelected : styles.autoCompleteText}`].join(' ')}>
                            //     {item.index == 0 ?
                            //         <div>{item.symbol}</div>
                            //         :
                            //         <div>{item.symbol} - {item.name}</div>
                            //     }
                            // </div>
                        )}
                    </div>
                    {/* } */}
                </div>
            </React.Fragment>
        );
    }
}

export default SelectOptionsList;
