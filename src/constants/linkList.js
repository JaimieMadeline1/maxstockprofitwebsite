const linkList = [
    { id: 0, linkName: 'Home', linkTo: '/' },
    { id: 1, linkName: 'Account', linkTo: '/account' },
    // { id: 2, linkName: 'Login', linkTo: '/login' },
    { id: 3, linkName: 'History', linkTo: '/history' },
];

export default linkList;