const TECHNICAL_NONE = 'NONE';
const TECHNICA_SMA = 'SMA';
const TECHNICA_EMA = 'EMA';
const TECHNICA_MMA = 'MMA';
const TECHNICA_BBands = 'BBands';

export {
    TECHNICAL_NONE,
    TECHNICA_SMA,
    TECHNICA_EMA,
    TECHNICA_MMA,
    TECHNICA_BBands
}