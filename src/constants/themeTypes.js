// const themeObj = {
//     DEFAULT: 'DEFAULT',
//     DARK_BLUE: 'DARK_BLUE',
//     LIGHT_BLUE: 'LIGHT_BLUE',
//     DARK_EARTH: 'DARK_EARTH',
//     LIGHT_EARTH: 'LIGHT_EARTH'
// }

const DEFAULT = 'DEFAULT';
const DARK_BLUE = 'DARK_BLUE';
const LIGHT_BLUE = 'LIGHT_BLUE';
const DARK_EARTH = 'DARK_EARTH';
const LIGHT_EARTH = 'LIGHT_EARTH';

export {
    DEFAULT,
    DARK_BLUE,
    LIGHT_BLUE,
    DARK_EARTH,
    LIGHT_EARTH
}

// export default {
//     themeObj
// }