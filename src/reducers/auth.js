import {
    AUTH_LOGIN,
    AUTH_LOGOUT
} from '../constants/actionTypes';

const intialState = { isLoggedIn: false, email: '', password: '' };

export const auth = (state = intialState, action) => {
    switch (action.type) {
        case AUTH_LOGIN:
            return { ...state, isLoggedIn: true, email: action.email, password: action.password };
            break;
        case AUTH_LOGOUT:
            return { ...state, isLoggedIn: false };
            break;
        default:
            return state;
    }
}