import {
    STAGE_TOOL_ADD_STATE
} from '../constants/actionTypes';

const intialState = { savedState: null, otherData: {} };

export const stageTool = (state = intialState, action) => {
    switch (action.type) {
        case STAGE_TOOL_ADD_STATE:
            return { ...state, savedState: action.state, otherData: action.otherData };
            break;
        default:
            return state;
    }
}