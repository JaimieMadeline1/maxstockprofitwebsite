import {
    CHANGE_STOCK_THEME
} from '../constants/actionTypes';

import {
    DEFAULT,
    DARK_BLUE,
    LIGHT_BLUE,
    DARK_EARTH,
    LIGHT_EARTH
} from '../constants/themeTypes';

const intialState = { currentTheme: DEFAULT };

export const stockChart = (state = intialState, action) => {
    switch (action.type) {
        case CHANGE_STOCK_THEME:
            return { ...state, currentTheme: action.currentTheme };
            break;
        default:
            return state;
    }
}