import {
    STOCK_CHART_PREFERNCES_ALTER_TYPE,
    STOCK_CHART_PREFERNCES_ALTER_NUMBER
} from '../constants/actionTypes';

const intialState = { typeOfInterval: 'Year', inputIntervalNumber: 4 };

export const stockChartPreferences = (state = intialState, action) => {
    switch (action.type) {
        case STOCK_CHART_PREFERNCES_ALTER_TYPE:
            return { ...state, typeOfInterval: action.intervalType };
            break;
        case STOCK_CHART_PREFERNCES_ALTER_NUMBER:
            return { ...state, inputIntervalNumber: action.number };
            break;
        default:
            return state;
    }
}