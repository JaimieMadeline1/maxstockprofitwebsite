import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { ClipLoader } from 'react-spinners';

import styles from './Contact.styl';
import colors from '../../colors/colors.styl';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
var image = require('../../assets/backgroundImg1.jpg');

class History extends React.Component {
    constructor(props) {
        super(props);
        this.state = { pastSearchs: this.props.pastStocks };
    }
    render() {
        return (
            <React.Fragment>
                <img className={styles.img} src={image} alt="background img" />
                <div className={styles.container}>
                    <h4 className={styles.box}>If you have any improvements and/or thoughts about the website's feature, please feel free to contact by email or share on media.</h4>
                    <h4 className={styles.box}>Vladimir.Malykh@yahoo.ca</h4>
                </div>
            </React.Fragment>
        );
    }
}


export default History;
