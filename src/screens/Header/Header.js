import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import styles from './Header.styl';
// import RNFirebaseLogo from '../../assets/RNFirebase.png';
const logo = require('../../assets/RNFirebase.png');
import Account from '../Account/Account';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

import { Link, withRouter } from "react-router-dom";
// import { browserHistory } from 'react-router'
// import createHistory from 'history/createBrowserHistory';

// Create a history of your choosing (we're using a browser history in this case)
// const history = createHistory();

const appConstants = require('../../constants/appConstants');
import { connect } from 'react-redux';
// import {
//     AUTH_PRELOG
// } from '../../constants/actionTypes';

class Header extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <React.Fragment>
                <div className={styles.heading}>
                    <div className={["container", styles.container].join(' ')}>
                        <div className="row">
                            {/* <Link className={["col-lg-2", "col-md-2", "col-xs-12"].join(' ')} to="./">
                                <img width="50" src={logo} alt="logo" />
                            </Link> */}
                            <Link className={[styles.headerText, styles.textMarginTop, styles.customPaddingLeft, "col-lg-5", "col-sm-12"].join(' ')} to="./">
                                <h5 style={{ color: '#007bff' }}>{appConstants.APP_NAME}</h5>
                            </Link>
                            {/* <button className={[styles.logInButton, "col-lg-2"].join(' ')}
                                onClick={this.changeLogInState()}>{this.props.isloggedIn == false ? 'Log In' : 'Log Out'}</button> */}
                            <div className="col-lg-1"></div>
                            <div className={["col-lg-6", "col-sm-12", styles.rightHeader].join(' ')}>
                                {/* <div>Contact</div> */}
                                {/* {this.props.isloggedIn == false ?
                                    <Link to="./login" className={[styles.loginProfile].join(' ')}>
                                        <Button color="primary" size="md" style={{ whiteSpace: 'normal' }}
                                            onClick={() => { }}>Log In</Button>
                                    </Link>
                                    : */}
                                <div className={[].join(' ')} >
                                    <div className={[styles.routes].join(' ')}>
                                        <Link to="./history">
                                            <Button color="primary" size="md" style={{ whiteSpace: 'normal' }}>
                                                {/* <h5> */}
                                                History
                                            {/* </h5> */}
                                            </Button>
                                        </Link>
                                    </div>
                                    <div className={[styles.routes].join(' ')}>
                                        <Link to="./contact">
                                            <Button color="primary" size="md" style={{ whiteSpace: 'normal' }}>
                                                {/* <h5> */}
                                                Contact
                                            {/* </h5> */}
                                            </Button>
                                        </Link>
                                    </div>
                                    {/* <div className={styles.routes}>
                                        <Link to="./account">
                                            <Button color="primary" size="md" style={{ whiteSpace: 'normal' }}
                                                onClick={() => { }}>{this.props.userStateName != '' ? this.props.userStateName : 'No name given.'}</Button>
                                        </Link>
                                    </div> */}
                                </div>
                                {/* } */}
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment >
        );
    }
}

const mapStateToProps = (state) => {
    return {
        isloggedIn: state.auth.isLoggedIn,
        userStateName: state.userState.userName,
    }
}

export default withRouter(connect(mapStateToProps)(Header));
