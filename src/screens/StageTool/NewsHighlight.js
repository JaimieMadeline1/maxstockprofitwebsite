import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import styles from './NewsHighlight.styl';
import stylesOverlay from './overlay.styl';
import colors from '../../colors/colors.styl';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import axios from 'axios';
const newsConstants = require('../../constants/newsConstants');
import { ClipLoader } from 'react-spinners';

class NewsHighlight extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            stockName: props.stockName,
            newsNewsApiData: [],
            isLoadingNews: false,
            hidePartly: true
        };
    }
    componentDidMount() {
        // this.props.loadingOn();
        this.setState({ isLoadingNews: true });

        // Promise.all([this.getNewsApi()])
        //     .then(() => {
        //         this.props.loadingOff();
        //     }).catch(() => {
        //         this.props.loadingOff();
        //     })
        this.getNewsApi();
    }
    componentWillReceiveProps(props) {
        if (this.state.stockName != props.stockName) {
            this.setState({
                stockName: props.stockName,
            }, () => {
                this.getNewsApi();
            });
        }
    }
    formatDateCustom(date) {
        var options = { hour: 'numeric', minute: 'numeric', day: 'numeric', month: 'long', year: 'numeric' };
        return new Date(date).toLocaleDateString("en-US", options)
    }
    compare = (a, b) => {
        if (new Date(a.id).getTime() < new Date(b.id).getTime())
            return 1;
        if (new Date(a.id).getTime() > new Date(b.id).getTime())
            return -1;
        return 0;
    }
    getNewsApi() {
        // NewsAPI
        var url = 'https://newsapi.org/v2/everything?' +
            `q=${this.state.stockName}&` +
            //   'from=2018-05-06&' +
            'sortBy=popularity&' +
            `apiKey=${newsConstants.NEWSAPI_KEY}`;
        console.log(url);
        axios.get(url)
            .then((response) => {
                console.log(response.data.articles);
                let dataTemp = [];
                response.data.articles.forEach((item) => {
                    // let date = new Date(item.published).toDateString();
                    let date = this.formatDateCustom(item.publishedAt);
                    dataTemp.push({
                        id: item.publishedAt,
                        text: item.description,
                        title: item.title,
                        source: item.source.name,
                        author: item.author,
                        publishedDate: date,
                        image: item.urlToImage
                    });
                });
                console.log(dataTemp);
                dataTemp.sort(this.compare);
                console.log(dataTemp);
                this.setState({ newsNewsApiData: dataTemp });
            })
            // .catch((error) => {
            //     // console.log(error);
            // })
            .finally(() => {
                this.turnOffSpinner();
            });
    }
    onClickPartlyVisible = () => {
        // console.log("trigger");
        this.setState({ hidePartly: false });
    }
    turnOffSpinner() {
        this.setState({ isLoadingNews: false });
    }
    render() {
        return (
            <React.Fragment>
                <div className={styles.container}>
                    <div>
                        {/* <div className={styles.testStyle}>NewsApi</div> */}
                        {this.state.newsNewsApiData.length != 0 ?
                            <div className={this.state.hidePartly ?
                             styles.innerContainer : [styles.tableContainer, styles.scrollVertically].join(" ")}>
                                {this.state.newsNewsApiData.map((story) =>
                                    <div key={story.id}>
                                        {/* {story.language == 'english' && */}
                                        <div className={styles.eachStory}>
                                            {story.image != null && story.image != '' ?
                                                <img src={story.image} className={styles.image} />
                                                :
                                                <div>
                                                </div>
                                            }
                                            <div className={styles.storyTitle}>{story.title}</div>
                                            <br />
                                            <div>{story.publishedDate}</div>
                                            <br />
                                            <div className={styles.longText}>{story.text}</div>
                                        </div>
                                        {/* } */}
                                    </div>
                                )}
                            </div>
                            :
                            <div>
                                <h5>Noting Selected at the moment.</h5>
                            </div>
                        }
                        {this.state.isLoadingNews == true &&
                            <div className="sweetLoading">
                                <ClipLoader
                                    color={colors.spinner}
                                    size={32}
                                    loading={this.state.isLoadingNews}
                                />
                            </div>
                        }
                    </div>
                    {this.state.hidePartly &&
                        <React.Fragment>
                            <div onClick={this.onClickPartlyVisible} className={stylesOverlay.overlay}>
                                <div></div>
                            </div>
                        </React.Fragment>
                    }
                </div>
            </React.Fragment>
        );
    }
}

export default NewsHighlight;
