import React from 'react';
// import ReactDOM from 'react-dom';
// import { ClipLoader } from 'react-spinners';

import styles from './PositionTable.styl';
import stylesOverlay from './overlay.styl';
import colors from '../../colors/colors.styl';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
// import { connect } from 'react-redux';
// import {
//     HISTORY_CLEAR
// } from '../../constants/actionTypes';

import BootstrapTable from 'react-bootstrap-table-next';

// dateModifier: dateModifier, importance: differencePercent - (differencePercent * dateModifier / (timePerTrade * 2)),
//                         difference: differencePercent, date: criticalPointsDataTemp[i].date, close: criticalPointsDataTemp[i].value, numberInArray: i
// current Price,


class PositionTable extends React.Component {
    constructor(props) {
        super(props);
        let initialList = this.props.positionList;
        console.log("initlist");
        console.log(initialList);
        // console.log(initialList);
        // this.props.pastStocks.forEach((item) => {
        //     newList.push({
        //         stockName: item.stockName.toUpperCase(), time: new Date(item.time).toLocaleString()
        //     });
        // })
        this.columns = [
            {
                dataField: 'dateRange',
                text: 'Date Range',
                sort: true
                // classes: this.rowClassHelper
            },
            {
                dataField: 'daysApart',
                text: 'Date Range (In Days)',
                sort: true
            },
            {
                dataField: 'positionExact',
                text: 'Position',
                sort: true
            },
            {
                dataField: 'profitPercent',
                text: 'Profits',
                sort: true
            },
            {
                dataField: 'obj.close',
                text: 'Starting Close',
                sort: true
            },
            {
                dataField: 'startClose',
                text: 'Ending Close',
                sort: true
            }
        ];

        this.state = {
            data: initialList,
            hidePartly: true
            // finishSelectingTickerSymbol: false, searchSuggestionSelectedIndex: 0,
            // tempSearchTickerSuggestions: [], inputTickerfocused: true
        };

        // this.tempSearchTickerSuggestions = [];
    }
    componentWillReceiveProps(props) {
        this.setState({
            data: props.positionList,
        });
    }
    deleteItem() {
        // this.props.clearHistory();
        // this.setState({ tempPastSearchs: [], pastSearchs: [] });
    }
    onTickerInputChange(e) {
        // this.setState({ tickerInput: e.target.value });
        // // tempPastSearchs
        // let tempData = this.state.pastSearchs.filter((el) => {
        //     // this.filterHelper(el);
        //     return el.stockName.toLowerCase().indexOf(e.target.value.toLowerCase()) > -1;
        // });
        // this.setState({ tempPastSearchs: tempData });
        // this.handleTickerOptions(e.target.value);
    }
    // rowClassHelper(cell, row, rowIndex, colIndex) {
    //     if (rowIndex % 2 === 0) return styles.demoRowEven;
    //     return styles.demoRowOdd;
    // }
    rowClasses = (row, rowIndex) => {
        // console.log(row);hidePartly
        if (this.state.hidePartly) {
            if (rowIndex <= 4) {
                if (row.isHold) {
                    return styles.positionHold;
                } else if (row.position == 'long') {
                    return styles.positionLong;
                } else if (row.position == 'short') {
                    return styles.positionShort;
                }
            } else {
                return styles.limitTable;
            }
        } else {
            if (row.isHold) {
                return styles.positionHold;
            } else if (row.position == 'long') {
                return styles.positionLong;
            } else if (row.position == 'short') {
                return styles.positionShort;
            }
        }
        // limitTable
    }
    onClickPartlyVisible = () => {
        console.log("trigger");
        this.setState({ hidePartly: false });
    }
    render() {
        return (
            <React.Fragment>
                <div className={styles.container}>
                    {/* <div className={styles.selections}> */}
                    {/* <Button color="primary" size="md" style={{ whiteSpace: 'normal' }}
                                onClick={() => {
                                    this.clearHistory()
                                }}>
                                Clear History
                            </Button> */}
                    {/* <Input
                            autoFocus
                            spellCheck="false"
                            // onKeyDown={(event) => this.handleKeyPress(event)}
                            className={[styles.inputTicker, styles.uppercase].join(' ')}
                            value={this.state.tickerInput}
                            placeholder='Filter By Ticker'
                            onChange={(e) => this.onTickerInputChange(e)}
                            type="text" /> */}
                    {/* </div> */}
                    <div className={styles.mainContainer}>
                        {this.state.data.length > 0 ?
                            <div>
                                <div className={this.state.hidePartly ? styles.tableContainer : [styles.tableContainer, styles.scrollVertically].join(" ")}>
                                    <BootstrapTable style={{ height: "300px" }} hover bordered keyField="obj.date"
                                        data={this.state.data} columns={this.columns} rowClasses={this.rowClasses} />
                                </div>
                                {this.state.hidePartly &&
                                    <React.Fragment>
                                        <div onClick={this.onClickPartlyVisible} className={stylesOverlay.overlay}>
                                            <div></div>
                                        </div>
                                        {/* <div className={styles.iconStyle}><FontAwesomeIcon icon={faChevronDown} /></div> */}
                                    </React.Fragment>
                                }
                            </div>
                            :
                            <div>
                                <h4>There are no stocks selected.</h4>
                            </div>
                        }
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default PositionTable;
