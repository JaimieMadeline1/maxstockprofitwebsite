import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import styles from './StageTool.styl';
import colors from '../../colors/colors.styl';
import { Button, Form, FormGroup, Label, Input, FormText, Alert } from 'reactstrap';
import { TypeChooser } from "react-stockcharts/lib/helper";
import Chart from '../../charts/Chart';
import { DateRangePicker, SingleDatePicker, DayPickerRangeController } from 'react-dates';
import 'react-dates/initialize';
import { InputNumber, Select } from 'antd';
// import { gql, withApollo } from 'react-apollo';
// import { getData } from "../../charts/utils"
import { ClipLoader } from 'react-spinners';
// import AnyChart from 'anychart-react/dist/anychart-react.min.js';
import anychart from 'anychart';

import axios from 'axios';

const tickersList = require('../../data/tickerSearch');
const alphaVantage = require('../../constants/alphaVantage');
import _ from 'lodash';

import { connect } from 'react-redux';
import {
    HISTORY_ADD_SEARCH,
    CHANGE_STOCK_THEME,
    STAGE_TOOL_ADD_STATE,
    STOCK_CHART_PREFERNCES_ALTER_NUMBER,
    STOCK_CHART_PREFERNCES_ALTER_TYPE

} from '../../constants/actionTypes';

import { monthNames } from '../../constants/monthNames';

import SelectOptionsList from '../../components/AutoComplete/SelectOptionsList';
// import StockHolder from './StockHolder';
import NewsHighlight from './NewsHighlight';
import PositionTable from './PositionTable';

import loadChart from './services/loadChart';
import calcChartData from './services/calcChartData';

import {
    DEFAULT,
    DARK_BLUE,
    LIGHT_BLUE,
    DARK_EARTH,
    LIGHT_EARTH
} from '../../constants/themeTypes';

import {
    TECHNICAL_NONE,
    TECHNICA_SMA,
    TECHNICA_EMA,
    TECHNICA_MMA,
    TECHNICA_BBands
} from '../../constants/technicalTypes';

class StageTool extends React.Component {
    constructor(props) {
        super(props);
        // console.log(this.props.savedState);
        // console.log(this.props.otherData);
        // if (this.props.savedState != null) {
        //     this.state = this.props.savedState;

        //     this.currentData = this.props.otherData.currentData; // save this
        //     this.inputTechnicalNumber = this.props.otherData.inputTechnicalNumber; // save this
        //     // this.typeOfChartTheme = this.props.currentTheme; // save this
        //     this.typeOfTechnicalIndicator = this.props.otherData.typeOfTechnicalIndicator; // save this
        //     this.measuedPositionList = this.props.otherData.measuedPositionList; // save this
        //     this.currentStockName = this.props.otherData.currentStockName; // save this
        //     this.currentCompanyName = this.props.otherData.currentCompanyName; // save this
        //     this.finalMeasuedData = this.props.otherData.finalMeasuedData; // save
        //     this.measuredDataDifference = this.props.otherData.measuredDataDifference; // save
        //     this.isLoadChartUponMount = true;
        // } else {
        this.state = {
            tickerList: [], disabledButtons: false, tickerInput: '', searchTickerSuggestions: [],
            searchSuggestionSelectedIndex: -1, inputTickerfocused: false, finishSelectingTickerSymbol: false,
            chartData: [], stockPrice: '', focusedInput: null, startDate: null, endDate: null, inputIntervalNumber: this.props.stockChartPreferencesNumber,
            typeOfInterval: this.props.stockChartPreferencesType, loadingStockData: false, tickerList: [], showNumberSelector: false, inputTechnicalNumber: 20,
            finalPositionData: [], isCantFindTicker: false, typeOfChartTheme: this.props.currentTheme, maxNumberInterval: 100,
            isShowRefresh: false, avoidVolalityNumber: 0, avoidVolalityDaysNearby: 5, newsStockName: ''
        };
        this.currentData = []; // save this
        this.inputTechnicalNumber = 20; // save this
        // this.typeOfChartTheme = this.props.currentTheme; // save this
        this.typeOfTechnicalIndicator = TECHNICAL_NONE; // save this
        this.measuedPositionList = []; // save this
        this.finalMeasuedData = []; // save
        this.measuredDataDifference = []; // save
        // }
        this.tempSearchTickerSuggestions = [];
        this.isPreventBlur = false;
        this.wInstance = null;
        // this.measuredDataTemp = [];

        this.webWorkers = {
            workerCalcChartData: './workers/calcChartDataWorker.js'
        }
    }
    // componentDidMount() {
    // if (this.isLoadChartUponMount && this.currentData && this.currentStockName) {
    //     this.loadChart(this.currentData, this.currentStockName, this.currentCompanyName);
    // }
    // }
    // componentWillUnmount() {
    //     this.saveCurrentStateBackup();
    // }
    loadChart(params) {
        loadChart(params);
        this.setState({ loadingStockData: false });
    }
    calcChartData(params) {
        // check for web worker support
        if (typeof (Worker) !== "undefined") {
            // check if instance is already there
            if (this.wInstance == null) {
                this.wInstance = new Worker(this.webWorkers.workerCalcChartData);
                this.wInstance.postMessage(params);
            } else {
                // wait for current worker to finish
                // optionally compare matching process id
            }
            this.wInstance.onmessage = (event) => {
                // console.log("Response");
                // console.log(event);
                console.log(event.data);
                if (this.wInstance != null) {
                    this.wInstance.terminate();
                    this.wInstance = null;
                    this.parseIntoAnyChart(event.data);
                }
            };
        } else {
            console.log("Lack of web worker support.");
        }
        // calcChartData(params);
    }
    saveCurrentStateBackup() {
        let otherData = {
            currentData: this.currentData,
            inputTechnicalNumber: this.inputTechnicalNumber,
            // typeOfChartTheme: this.typeOfChartTheme,
            typeOfTechnicalIndicator: this.typeOfTechnicalIndicator,
            measuedPositionList: this.measuedPositionList,
            currentStockName: this.currentStockName,
            currentCompanyName: this.currentCompanyName,
            finalMeasuedData: this.finalMeasuedData,
            measuredDataDifference: this.measuredDataDifference
        };
        this.props.saveCurrentState(this.state, otherData);
    }

    calculate(valueFromSuggestion, companyName) {
        // this.setState({ disabledButtons: true });
        this.getTickerPrice(valueFromSuggestion, companyName);
    }
    calculateHelper(dataItems, stockName, companyName) {
        console.log("calculating");
        if (dataItems != null && Object.keys(dataItems).length > 0) {
            let dataTemp = [];
            Object.keys(dataItems).forEach((item) => {
                // console.log(item);
                // console.log(new Date(item).getTime());
                if (item) {
                    dataTemp.push({
                        // date: new Date(item),
                        date: item,
                        open: dataItems[item]['1. open'],
                        high: dataItems[item]['2. high'],
                        low: dataItems[item]['3. low'],
                        close: dataItems[item]['4. close'],
                        value: dataItems[item]['5. volume'],
                    });
                }
            });
            this.dataTemp = dataTemp;
            this.parseDataHelper(dataTemp, stockName, companyName);
        }
    }
    parseDataHelper(dataTemp, stockName, companyName) {
        this.setState({ isShowRefresh: false });
        if (this.state.inputIntervalNumber > this.state.maxNumberInterval) {
            this.setState({ inputIntervalNumber: this.state.maxNumberInterval }, () => {
                this.parseIntoAnyChartOptionObjectBuilder(dataTemp, stockName, companyName);
            });
        } else {
            this.parseIntoAnyChartOptionObjectBuilder(dataTemp, stockName, companyName);
        }
    }
    parseIntoAnyChartOptionObjectBuilder(dataTemp, stockName, companyName) {
        this.setState({ loadingStockData: false });

        // dataTemp, stockName, companyName, typeOfInterval, inputIntervalNumber, avoidVolalityNumber, avoidVolalityDaysNearby
        // Synchronous Call
        // this.parseIntoAnyChart(calcChartData({
        //     dataTemp, stockName, companyName, typeOfInterval: this.state.typeOfInterval,
        //     inputIntervalNumber: this.state.inputIntervalNumber,
        //     avoidVolalityNumber: this.state.avoidVolalityNumber, avoidVolalityDaysNearby: this.state.avoidVolalityDaysNearby
        // }));

        // Asynchronous Call
        this.calcChartData({
            dataTemp, stockName, companyName, typeOfInterval: this.state.typeOfInterval,
            inputIntervalNumber: this.state.inputIntervalNumber,
            avoidVolalityNumber: this.state.avoidVolalityNumber, avoidVolalityDaysNearby: this.state.avoidVolalityDaysNearby
        });
    }
    parseIntoAnyChart(parsingDataResult) {
        if (parsingDataResult) {
            let { finalMeasuedData, tempList, measuredDataDifference, dataTemp, stockName, companyName } = parsingDataResult;

            this.finalMeasuedData = finalMeasuedData; // save
            this.measuedPositionList = tempList;
            this.setState({ finalPositionData: tempList });
            this.measuredDataDifference = measuredDataDifference; // save
            // this.measuredDataTemp = measuredDataTemp;
            this.currentData = dataTemp;
            this.currentStockName = stockName;
            this.currentCompanyName = companyName;
            this.loadChart({
                stockData: dataTemp, stockName, companyName, typeOfChartTheme: this.state.typeOfChartTheme, typeOfTechnicalIndicator: this.typeOfTechnicalIndicator,
                inputTechnicalNumber: this.inputTechnicalNumber, measuredDataDifference: this.measuredDataDifference,
                finalMeasuedData: this.finalMeasuedData, measuedPositionList: this.measuedPositionList
            });
            this.setState(prevState => {
                return { tickerList: [...prevState.tickerList, { stockName: stockName, companyName: companyName, riskRating: 80 }] }
            });
            // add stock name to history
            let currDate = new Date();
            // console.log( { stockName: stockName, time: currDate.getTime()} );
            this.props.historyAddSearch({ stockName: stockName, companyName: companyName, time: currDate.getTime() });
            // load off spinner
            // }
            console.log("done");
            this.setState({ loadingStockData: false });
        }
    }
    getTickerPrice(stockName, companyName) {
        // https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=MSFT&interval=15min&outputsize=full&apikey=demo

        // Intraday 30 Min past 10-15 days
        // axios.get(`https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=${stockName}&interval=30min&outputsize=full&apikey=${alphaVantage.USER_KEY}`)
        //     .then((response) => {
        //         console.log(response);
        //         this.parseAlphaVantageData(response.data["Time Series (30min)"], stockName, companyName);
        //     })
        //     .catch((error) => {
        //         // console.log(error);
        //     });

        // Daily past 20 years
        // https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=MSFT&outputsize=full&apikey=demo
        axios.get(`https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=${stockName}&outputsize=full&apikey=${alphaVantage.USER_KEY}`)
            .then((response) => {
                console.log(response);
                if (response.data && response.data['Error Message']) {
                    this.setState({ isCantFindTicker: true, loadingStockData: false });
                } else {
                    this.setState({ isCantFindTicker: false });
                    this.calculateHelper(response.data["Time Series (Daily)"], stockName, companyName);
                    // this.parseAlphaVantageData(response.data["Time Series (Daily)"], stockName, companyName);
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }
    onTickerInputChange(e) {
        // console.log(e.target.value);
        if (e.target.value.length >= 1) {
            this.setState({ tickerInput: e.target.value, finishSelectingTickerSymbol: false, searchSuggestionSelectedIndex: 0 });
            this.handleTickerOptions(e.target.value);
        } else {
            this.setState({ tickerInput: e.target.value, finishSelectingTickerSymbol: true, searchSuggestionSelectedIndex: -1 });
        }
    }
    // There are roughly 8,614 symbols that this app recognizes
    // https://api.iextrading.com/1.0/ref-data/symbols
    handleTickerOptions(val) {
        this.tempSearchTickerSuggestions = [{
            index: 0,
            symbol: val.toUpperCase(),
            name: ''
        }];
        this.tempSearchTickerSuggestionsNotFiltered = [];
        let customIndex = 1;
        for (let i = 0; i < tickersList.tickerSearch.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            // if (this.tempSearchTickerSuggestions.length < 6) {
            if (tickersList.tickerSearch[i].symbol.substr(0, val.length).toUpperCase() == val.toUpperCase() ||
                (tickersList.tickerSearch[i].name && (tickersList.tickerSearch[i].name.toUpperCase().indexOf(val.toUpperCase())) > -1)) {
                let matchPercentage = 0;
                const tickerImportance = 0.5;
                const nameImportance = 0.5;

                // tickersList.tickerSearch[i].symbol.length / val.length * 0.50 + tickersList.tickerSearch[i].symbol.length / val.length * 0.5
                if (tickersList.tickerSearch[i].symbol.substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                    let temp = val.length / tickersList.tickerSearch[i].symbol.length * tickerImportance;
                    matchPercentage += temp;
                    if (temp === 0.5) {
                        this.tempSearchTickerSuggestions[0].name = tickersList.tickerSearch[i].name;
                    }
                }
                if (tickersList.tickerSearch[i].name && (tickersList.tickerSearch[i].name.toUpperCase().indexOf(val.toUpperCase())) > -1) {
                    let temp = val.length / tickersList.tickerSearch[i].name.length * nameImportance;
                    matchPercentage += temp;
                }

                this.tempSearchTickerSuggestionsNotFiltered.push({
                    // index: customIndex,
                    symbol: tickersList.tickerSearch[i].symbol.toUpperCase(),
                    name: tickersList.tickerSearch[i].name,
                    matchPercentage: matchPercentage
                });
                // customIndex++;
            }
            // } else {
            //     i = tickersList.tickerSearch.length;
            // }
        }
        // Sort by percentage of match
        // this.tempSearchTickerSuggestionsNotFiltered.sort((a, b) => {
        const compare = (a, b) => {
            if (a.matchPercentage < b.matchPercentage)
                return 1;
            if (a.matchPercentage > b.matchPercentage)
                return -1;
            return 0;
        }
        this.tempSearchTickerSuggestionsNotFiltered.sort(compare);
        // console.log(this.tempSearchTickerSuggestionsNotFiltered);

        // Take top 6.
        for (let i = 0; i < this.tempSearchTickerSuggestionsNotFiltered.length; i++) {
            if (this.tempSearchTickerSuggestions.length < 6) {
                this.tempSearchTickerSuggestions.push({
                    index: customIndex,
                    symbol: this.tempSearchTickerSuggestionsNotFiltered[i].symbol.toUpperCase(),
                    name: this.tempSearchTickerSuggestionsNotFiltered[i].name
                });
                customIndex++;
            } else {
                i = this.tempSearchTickerSuggestionsNotFiltered.length;
            }
        }

        // Update list
        this.setState({ searchTickerSuggestions: this.tempSearchTickerSuggestions });
    }
    finishSelectingTickerSymbol() {
        this.setState({ finishSelectingTickerSymbol: true });
    }
    userSelectedSymbol(index) {
        const valueFromSuggestion = this.state.searchTickerSuggestions[index].symbol;
        const companyName = this.state.searchTickerSuggestions[index].name;
        // console.log(this.state.searchTickerSuggestions[index]);
        this.setState({ tickerInput: valueFromSuggestion, newsStockName: valueFromSuggestion, loadingStockData: true });
        // this.handleTickerOptions(valueFromSuggestion);
        this.finishSelectingTickerSymbol();
        this.calculate(valueFromSuggestion, companyName);
    }
    userClickedSymbolSuggestion(index) {
        this.userSelectedSymbol(index);
    }
    userSelectedSymbolSuggestion(index) {
        this.userSelectedSymbol(index);
    }
    onBlur() {
        if (!this.isPreventBlur) {
            this.setState({ inputTickerfocused: false })
        }
    }
    onFocus() {
        this.setState({ inputTickerfocused: true })
    }
    handleKeyPress(e) {
        if (this.state.searchSuggestionSelectedIndex != -1) {
            // console.log(e.key);
            if (e.keyCode == 13) {
                this.userSelectedSymbolSuggestion(this.state.searchSuggestionSelectedIndex);
            }
            if (this.state.searchTickerSuggestions.length > 0) {
                if (e.keyCode == 38) {
                    this.setState(prevState => {
                        if (prevState.searchSuggestionSelectedIndex == 0) {
                            return ({ searchSuggestionSelectedIndex: this.state.searchTickerSuggestions.length - 1 })
                        } else {
                            return ({ searchSuggestionSelectedIndex: prevState.searchSuggestionSelectedIndex - 1 })
                        }
                    });
                }
                if (e.keyCode == 40) {
                    this.setState(prevState => {
                        if (prevState.searchSuggestionSelectedIndex == this.state.searchTickerSuggestions.length - 1) {
                            return ({ searchSuggestionSelectedIndex: 0 })
                        } else {
                            return ({ searchSuggestionSelectedIndex: prevState.searchSuggestionSelectedIndex + 1 })
                        }
                    });
                }
            }
        }
    }
    inputNumberAvoidVolatility = (value) => {
        console.log("this value is being refreshed.");
        this.setState({ avoidVolalityNumber: value, isShowRefresh: true });
    }
    inputNumberAvoidVolatilityDaysNearby = (value) => {
        this.setState({ avoidVolalityDaysNearby: value, isShowRefresh: true });
    }
    inputNumberChange = (value) => {
        // console.log(value);
        this.setState({
            inputIntervalNumber: Math.round(value),
            isShowRefresh: true
        });
        // this.setState({ isShowRefresh: true });
        this.props.alterChartIntervalNumber(value);
    }
    inputTechnicalNumberChange = (value) => {
        this.setState({ inputTechnicalNumber: value });
        this.inputTechnicalNumber = value;
        // if (value > 9) {
        this.loadChart({
            stockData: this.currentData, currentStockName: this.currentStockName,
            currentCompanyName: this.currentCompanyName, typeOfChartTheme: this.state.typeOfChartTheme, typeOfTechnicalIndicator: this.typeOfTechnicalIndicator,
            inputTechnicalNumber: this.inputTechnicalNumber, measuredDataDifference: this.measuredDataDifference,
            finalMeasuedData: this.finalMeasuedData, measuedPositionList: this.measuedPositionList
        });
        // }
    }
    handleTypeOfIntevalChange(value) {
        if (value == 'Month') {
            if (this.state.inputIntervalNumber > 6) {
                this.setState({ maxNumberInterval: 6, inputIntervalNumber: 6, typeOfInterval: 'Month' });
            } else {
                this.setState({ maxNumberInterval: 6, typeOfInterval: 'Month' });
            }
            this.props.alterChartIntervalType('Month');
        } else if (value == 'Year') {
            if (this.state.inputIntervalNumber > 100) {
                this.setState({ maxNumberInterval: 100, inputIntervalNumber: 100, typeOfInterval: 'Year' });
            } else {
                this.setState({ maxNumberInterval: 100, typeOfInterval: 'Year' });
            }
            this.props.alterChartIntervalType('Year');
        }
        this.setState({ isShowRefresh: true });
    }
    preventBlur() {
        this.isPreventBlur = true;
    }
    allowBlur() {
        this.isPreventBlur = false;
    }
    changeChartTheme(value) {
        this.setState({ typeOfChartTheme: value }, () => {
            this.loadChart({
                stockData: this.currentData, currentStockName: this.currentStockName,
                currentCompanyName: this.currentCompanyName, typeOfChartTheme: this.state.typeOfChartTheme, typeOfTechnicalIndicator: this.typeOfTechnicalIndicator,
                inputTechnicalNumber: this.inputTechnicalNumber, measuredDataDifference: this.measuredDataDifference,
                finalMeasuedData: this.finalMeasuedData, measuedPositionList: this.measuedPositionList
            });
        });
        this.props.changeStockChartTheme(value);
    }
    changeTechnicalIndicator(value) {
        if (value == TECHNICA_SMA || value == TECHNICA_EMA || value == TECHNICA_MMA) {
            this.setState({ showNumberSelector: true });
        } else {
            this.setState({ showNumberSelector: false });
        }
        this.typeOfTechnicalIndicator = value;
        this.loadChart({
            stockData: this.currentData, currentStockName: this.currentStockName,
            currentCompanyName: this.currentCompanyName, typeOfChartTheme: this.state.typeOfChartTheme, typeOfTechnicalIndicator: this.typeOfTechnicalIndicator,
            inputTechnicalNumber: this.inputTechnicalNumber, measuredDataDifference: this.measuredDataDifference,
            finalMeasuedData: this.finalMeasuedData, measuedPositionList: this.measuedPositionList
        });
    }
    refreshCalc(dataTemp, currentStockName, currentCompanyName) {
        this.setState({ loadingStockData: true, isShowRefresh: false }, () => {
            this.parseDataHelper(this.dataTemp, this.currentStockName, this.currentCompanyName);
        });
    }
    render() {
        return (
            <React.Fragment>
                <div className="sweetLoading">
                    <ClipLoader
                        color={colors.spinner}
                        size={32}
                        loading={this.state.loadingStockData}
                    />
                </div>
                <div className={["container-fluid", styles.mainContainer].join(' ')}>
                    <div className={["row", styles.tickerInputContainer].join(' ')}>
                        <React.Fragment>
                            <div className="col-lg-12">
                                <div className={["container-fluid"].join(' ')}>
                                    <div className={["row", styles.tickerInputContainer].join(' ')}>
                                        <div className="col-lg-3"></div>
                                        <div className={["col-lg-6", styles.mainInput].join(' ')}>
                                            <Input
                                                autoFocus
                                                spellCheck="false"
                                                onFocus={() => this.onFocus()}
                                                onBlur={() => this.onBlur()}
                                                onKeyDown={(event) => this.handleKeyPress(event)}
                                                className={[styles.inputTicker, styles.uppercase].join(' ')}
                                                value={this.state.tickerInput}
                                                placeholder='Enter Stock Ticker'
                                                onChange={(e) => this.onTickerInputChange(e)}
                                                type="text" />
                                            <SelectOptionsList
                                                isShow={(this.state.searchTickerSuggestions.length > 0 &&
                                                    this.state.finishSelectingTickerSymbol == false && this.state.inputTickerfocused == true)}
                                                searchSuggestions={this.state.searchTickerSuggestions}
                                                selectedIndex={this.state.searchSuggestionSelectedIndex}
                                                preventBlur={() => this.preventBlur()}
                                                allowBlur={() => this.allowBlur()}
                                                onClickEvent={(index) => this.userClickedSymbolSuggestion(index)}
                                            >
                                            </SelectOptionsList>
                                            {this.state.isCantFindTicker &&
                                                <Alert color="danger">
                                                    Can't find the entered stock.
                                                </Alert>
                                            }
                                            {this.state.isShowRefresh == true && this.state.tickerList.length > 0 &&
                                                <Button
                                                    disabled={this.state.disabledButtons}
                                                    className={styles.mainButton} outline color="primary" style={{ whiteSpace: 'normal' }}
                                                    onClick={() => this.refreshCalc(this.dataTemp, this.currentStockName, this.currentCompanyName)}>
                                                    Refresh
                                                </Button>
                                            }
                                            <div className={styles.intervalType}>
                                                <div className={styles.intervalTypeEach}>
                                                    <InputNumber step={1} min={1} max={this.state.maxNumberInterval}
                                                        value={this.state.inputIntervalNumber}
                                                        onChange={this.inputNumberChange} />
                                                </div>
                                                <div className={[styles.intervalTypeEach, styles.intervalTypeText].join(' ')}>{this.state.inputIntervalNumber > 1 ? 'Trades' : 'Trade'} Per</div>
                                                <div className={styles.intervalTypeEach}>
                                                    <Select defaultValue={this.state.typeOfInterval} style={{ width: 120 }} onChange={(value) => this.handleTypeOfIntevalChange(value)}>
                                                        {/* <Select.Option value="Weekly">Weekly</Select.Option> */}
                                                        <Select.Option value="Month">Month</Select.Option>
                                                        <Select.Option value="Year">Year</Select.Option>
                                                    </Select>
                                                </div>
                                            </div>
                                            <div className={styles.intervalType}>
                                                <div className={styles.intervalTypeEach} style={{ 'marginRight': '50px', 'marginLeft': '50px' }}>
                                                    <div>Algorithm Importance Rate to Avoid Volatiliy</div>
                                                    <InputNumber min={0} max={50}
                                                        value={this.state.avoidVolalityNumber}
                                                        onChange={this.inputNumberAvoidVolatility} />
                                                </div>
                                                <div className={styles.intervalTypeEach} style={{ 'marginRight': '50px', 'marginLeft': '50px' }}>
                                                    <div>Days Near Point to Avoid Volatility</div>
                                                    <InputNumber min={0} max={50}
                                                        value={this.state.avoidVolalityDaysNearby}
                                                        onChange={this.inputNumberAvoidVolatilityDaysNearby} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-3"></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-12">
                                <div style={this.state.tickerList.length < 1 ? { display: 'none' } : {}}>
                                    <div className={["container-fluid"].join(' ')}>
                                        <div className={["row", styles.tickerInputContainer].join(' ')}>
                                            <div className="col-lg-1"></div>
                                            <div className={["col-lg-3", styles.boxContainer].join(' ')}>
                                                <div>Technicals</div>
                                                <Select defaultValue={this.typeOfTechnicalIndicator} style={{ width: 120 }} onChange={(value) => this.changeTechnicalIndicator(value)}>
                                                    <Select.Option value={TECHNICAL_NONE}>None</Select.Option>
                                                    <Select.Option value={TECHNICA_SMA}>SMA</Select.Option>
                                                    <Select.Option value={TECHNICA_EMA}>EMA</Select.Option>
                                                    <Select.Option value={TECHNICA_MMA}>MMA</Select.Option>
                                                    <Select.Option value={TECHNICA_BBands}>Bollinger Bands (BBands)</Select.Option>
                                                </Select>
                                                {this.state.showNumberSelector &&
                                                    <InputNumber min={1} max={10000} defaultValue={this.state.inputTechnicalNumber} onChange={this.inputTechnicalNumberChange} />
                                                }
                                            </div>
                                            <div className="col-lg-4"></div>
                                            <div className={["col-lg-3", styles.boxContainer].join(' ')}>
                                                <div>Theme</div>
                                                <Select defaultValue={this.state.typeOfChartTheme} style={{ width: 120 }} onChange={(value) => this.changeChartTheme(value)}>
                                                    <Select.Option value={DEFAULT}>Default</Select.Option>
                                                    <Select.Option value={DARK_BLUE}>Dark Blue</Select.Option>
                                                    <Select.Option value={LIGHT_BLUE}>Light Blue</Select.Option>
                                                    <Select.Option value={DARK_EARTH}>Dark Earth</Select.Option>
                                                    <Select.Option value={LIGHT_EARTH}>Light Earth</Select.Option>
                                                </Select>
                                            </div>
                                            <div className="col-lg-1"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </React.Fragment>
                        {/* {this.state.chartData.length > 0 ? */}
                        <React.Fragment>
                            {/* {this.state.tickerList.length > 0 && */}
                            <React.Fragment>
                                <div style={this.state.tickerList.length < 1 ? { display: 'none' } : {}} className="col-lg-12">
                                    <div className={styles.chartContainer}>
                                        {/* {this.state.stockPrice != '' && <Label>Lastest Stock Price: {this.state.stockPrice}</Label>} */}
                                        <div>
                                            <div className={["container-fluid"].join(' ')}>
                                                <div className={["row"].join(' ')}>
                                                    <div id="container" className={["col-lg-6", styles.anychartContainer].join(' ')}></div>
                                                    <div id="container2" className={["col-lg-6", styles.anychartContainer].join(' ')}></div>
                                                    {/* <TypeChooser>
                                                    {type => <Chart type={type} data={this.state.chartData} tickerName={this.state.tickerInput} />}
                                                </TypeChooser> */}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {this.state.tickerList.length > 0 &&
                                    <React.Fragment>
                                        <div className="col-xl-6">
                                            <NewsHighlight stockName={this.state.newsStockName}></NewsHighlight>
                                        </div>
                                        <div className="col-xl-6">
                                            <PositionTable positionList={this.state.finalPositionData}></PositionTable>
                                        </div>
                                    </React.Fragment>
                                }
                            </React.Fragment>
                        </React.Fragment>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        currentTheme: state.stockChart.currentTheme,
        savedState: state.stageTool.savedState,
        otherData: state.stageTool.otherData,
        stockChartPreferencesType: state.stockChartPreferences.typeOfInterval,
        stockChartPreferencesNumber: state.stockChartPreferences.inputIntervalNumber,

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        historyAddSearch: (searchObj) => dispatch({ type: HISTORY_ADD_SEARCH, searchObj }),
        changeStockChartTheme: (theme) => dispatch({ type: CHANGE_STOCK_THEME, currentTheme: theme }),
        saveCurrentState: (state, otherData) => dispatch({ type: STAGE_TOOL_ADD_STATE, state: state, otherData: otherData }),
        alterChartIntervalType: (intervalType) => dispatch({ type: STOCK_CHART_PREFERNCES_ALTER_TYPE, intervalType: intervalType }),
        alterChartIntervalNumber: (number) => dispatch({ type: STOCK_CHART_PREFERNCES_ALTER_NUMBER, number: number })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StageTool);
