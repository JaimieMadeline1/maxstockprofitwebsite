import React, { Component } from 'react';
import ReactDOM from 'react-dom';
// import { ClipLoader } from 'react-spinners';

import styles from './StockHolder.styl';
import colors from '../../colors/colors.styl';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

// import { connect } from 'react-redux';
// import {
//     HISTORY_CLEAR
// } from '../../constants/actionTypes';

import BootstrapTable from 'react-bootstrap-table-next';

const columns = [{
    dataField: 'stockName',
    text: 'Ticker',
    sort: true
},
{
    dataField: 'companyName',
    text: 'Company Name',
    sort: true
},
{
    dataField: 'riskRating',
    text: 'Risk (%)',
    sort: true
}];

class StockHolder extends React.Component {
    constructor(props) {
        super(props);
        let initialList = this.props.tickerList;
        // console.log(initialList);
        // this.props.pastStocks.forEach((item) => {
        //     newList.push({
        //         stockName: item.stockName.toUpperCase(), time: new Date(item.time).toLocaleString()
        //     });
        // })

        this.state = {
            data: initialList
            // finishSelectingTickerSymbol: false, searchSuggestionSelectedIndex: 0,
            // tempSearchTickerSuggestions: [], inputTickerfocused: true
        };

        // this.tempSearchTickerSuggestions = [];
    }
    componentWillReceiveProps(props) {
        this.setState({
            data: props.tickerList,
        });
    }
    deleteItem() {
        // this.props.clearHistory();
        // this.setState({ tempPastSearchs: [], pastSearchs: [] });
    }
    onTickerInputChange(e) {
        // this.setState({ tickerInput: e.target.value });
        // // tempPastSearchs
        // let tempData = this.state.pastSearchs.filter((el) => {
        //     // this.filterHelper(el);
        //     return el.stockName.toLowerCase().indexOf(e.target.value.toLowerCase()) > -1;
        // });
        // this.setState({ tempPastSearchs: tempData });
        // this.handleTickerOptions(e.target.value);
    }
    render() {
        return (
            <React.Fragment>
                <div className={styles.container}>
                    <div className={styles.selections}>
                        {/* <Button color="primary" size="md" style={{ whiteSpace: 'normal' }}
                                onClick={() => {
                                    this.clearHistory()
                                }}>
                                Clear History
                            </Button> */}
                        {/* <Input
                            autoFocus
                            spellCheck="false"
                            // onKeyDown={(event) => this.handleKeyPress(event)}
                            className={[styles.inputTicker, styles.uppercase].join(' ')}
                            value={this.state.tickerInput}
                            placeholder='Filter By Ticker'
                            onChange={(e) => this.onTickerInputChange(e)}
                            type="text" /> */}
                    </div>
                    <div className={styles.mainContainer}>
                        {this.state.data.length > 0 ?
                            <div>
                                <div className={styles.tableContainer}>
                                    <BootstrapTable striped hover bordered keyField='stockName' data={this.state.data} columns={columns} />
                                </div>
                            </div>
                            :
                            <div>
                                <h4>There are no stocks selected.</h4>
                            </div>
                        }
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default StockHolder;
