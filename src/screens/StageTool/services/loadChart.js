import {
    DEFAULT,
    DARK_BLUE,
    LIGHT_BLUE,
    DARK_EARTH,
    LIGHT_EARTH
} from '../../../constants/themeTypes';

import {
    TECHNICAL_NONE,
    TECHNICA_SMA,
    TECHNICA_EMA,
    TECHNICA_MMA,
    TECHNICA_BBands
} from '../../../constants/technicalTypes';

const loadChart = (loadChartParam) => {
    console.log(loadChartParam);
    const { stockData, stockName, companyName, typeOfChartTheme, 
        typeOfTechnicalIndicator, inputTechnicalNumber, measuredDataDifference, finalMeasuedData,
        measuedPositionList } = loadChartParam;
    // let {...loadChartParam};

    let table = anychart.data.table('date');
    let tempA = [];
    // let isDateMatch = false;
    for (let i = 0; i < stockData.length; i++) {
        // isDateMatch = false;
        // for (let t = 0; t < this.finalMeasuedData.length; t++) {
        //     if (this.finalMeasuedData[t].date == stockData[i].date) {
        //         isDateMatch = true;
        //         tempA.push({
        //             'date': stockData[i].date, 'close': stockData[i].close, "volume": stockData[i].value, 'close2': this.finalMeasuedData[t].close,
        //         });
        //         t = this.finalMeasuedData.length;
        //     }
        // }
        // if (!isDateMatch) {
        tempA.push({
            'date': stockData[i].date, 'close': stockData[i].close, "volume": stockData[i].value,
        });
        // }
    }

    table.addData(
        tempA
    );

    // map the data
    let mapping = table.mapAs({ 'date': 'date', 'value': 'volume' });
    let mapping0 = table.mapAs({ 'value': 'close' });

    // console.log("Changing Theme to " + this.typeOfChartTheme);
    switch (typeOfChartTheme) {
        case DEFAULT:
            anychart.theme('');
            break;
        case DARK_BLUE:
            anychart.theme('darkBlue');
            break;
        case LIGHT_BLUE:
            anychart.theme('lightBlue');
            break;
        case DARK_EARTH:
            anychart.theme('darkEarth');
            break;
        case LIGHT_EARTH:
            anychart.theme('lightEarth');
            break;
        default:
        // anychart.theme('darkBlue');
    }
    // create stock chart
    var chart = anychart.stock();

    // set chart title
    chart.title(companyName);

    // create first plot on the chart and set settings
    var plot = chart.plot(0);
    plot.height('75%')
        .yGrid(true)
        .yMinorGrid(true);

    // create candlestick series
    var series = plot.line(mapping0);
    series.name(stockName);
    series.legendItem().iconType('rising-falling');

    // set max labels settings
    series.maxLabels()
        .enabled(true)
        .position('high')
        .fontWeight('bold')
    // .fontColor('#6da632');

    // set min labels settings
    series.minLabels()
        .enabled(true)
        .position('low')
        .fontWeight('bold')
    // .fontColor('#d81e05');

    switch (typeOfTechnicalIndicator) {
        case TECHNICAL_NONE:
            break;
        case TECHNICA_SMA:
            var sma20 = plot.sma(mapping0, inputTechnicalNumber).series();
            sma20.name('SMA(20)')
                .stroke('#bf360c');
            break;
        case TECHNICA_EMA:
            var ema50 = plot.ema(mapping0, inputTechnicalNumber).series();
            ema50.stroke('#ffa000');
            break;
        case TECHNICA_MMA:
            var mmaFirst = plot.mma(mapping0, inputTechnicalNumber).series();
            mmaFirst.stroke('1.5 #9170CB');
            break;
        case TECHNICA_BBands:
            // create BBands indicator with period 20
            var bBandsIndicator = plot.bbands(mapping0);
            bBandsIndicator.upperSeries().stroke('1.5 #3C8AD8');
            bBandsIndicator.middleSeries().stroke('1.5 #3C8AD8');
            bBandsIndicator.lowerSeries().stroke('1.5 #3C8AD8');
            break;
        default:
    }

    // var rsi = plot.rsi(mapping, 14).series();
    // rsi.stroke('#64b5f6');

    // mapping2
    // set settings for event markers
    var eventMarkers = plot.eventMarkers();
    // set markers data
    // this.measuredDataDifference
    eventMarkers.data([
        { date: measuredDataDifference[0].date, description: 'Highest Closing Price, ' + measuredDataDifference[0].close },
        // { date: this.measuredDataDifference[0].date, description: 'Highest Closing Price, ' + this.measuredDataDifference[0].close + ', ' + (Math.round(this.measuredDataDifference[0].difference * 100) / 100) + "x of intial price" },

    ]);

    // create second plot
    var volumePlot = chart.plot(1);
    // set yAxis labels formatter
    volumePlot.yAxis().labels().format('{%Value}{scale:(1000)(1)|(k)}');
    // set crosshair y-label formatter
    volumePlot.crosshair().yLabel().format('{%Value}{scale:(1000)(1)|(k)}');

    // create volume series on the plot
    var volumeSeries = volumePlot.column(mapping);
    // set series settings
    volumeSeries.name('Volume');

    // create scroller series with mapped data
    chart.scroller().area(mapping);


    // Graph 2 starts here
    let table2 = anychart.data.table('date');
    // this.finalMeasuedData
    let tempB = [];
    for (let i = 0; i < finalMeasuedData.length; i++) {
        tempB.push({
            'date': finalMeasuedData[i].date, 'close2': finalMeasuedData[i].close
        });
    }

    tempB.reverse();

    table2.addData(
        tempB
    );

    // Long
    let tableLong = anychart.data.table('date');
    // this.finalMeasuedData this.measuedPositionList
    let tempLong = [];
    for (let i = 0; i < measuedPositionList.length; i++) {
        if (measuedPositionList[i].position == 'long') {
            tempLong.push({
                'date': measuedPositionList[i].obj.date, 'close2': measuedPositionList[i].obj.close
            });
        }
    }

    tempLong.reverse();

    tableLong.addData(
        tempLong
    );

    // Short
    let tableShort = anychart.data.table('date');
    // this.finalMeasuedData this.measuedPositionList
    let tempShort = [];
    for (let i = 0; i < measuedPositionList.length; i++) {
        if (measuedPositionList[i].position == 'short') {
            tempShort.push({
                'date': measuedPositionList[i].obj.date, 'close2': measuedPositionList[i].obj.close
            });
        }
    }

    tempShort.reverse();

    tableShort.addData(
        tempShort
    );

    // map the data
    let mapping22 = table2.mapAs({ 'date': 'date', 'value': 'close2' });
    let mapping02 = table2.mapAs({ 'value': 'close2' });
    let mappingShort = tableShort.mapAs({ 'value': 'close2' });
    let mappingLong = tableLong.mapAs({ 'value': 'close2' });

    // create stock chart
    var chart2 = anychart.stock();

    // set chart title
    chart2.title(companyName + " Position Chart");

    // create first plot on the chart and set settings
    var plot2 = chart2.plot(0);
    plot2.height('100%')
        .yGrid(true)
        .yMinorGrid(true);

    // var series2 = plot2.jumpLine(mapping02);
    // var series2 = plot2.jumpLine(mapping02);
    // series2.name('Positions');
    // series2.legendItem().iconType('rising-falling');

    // jumpline short
    var seriesShort = plot2.jumpLine(mappingShort);
    seriesShort.name('Short Position');
    // seriesShort.stroke('#b90000');
    seriesShort.stroke("#b90000", 2, "0");
    seriesShort.legendItem().iconType('rising-falling');

    // jumpline long
    var seriesLong = plot2.jumpLine(mappingLong);
    seriesLong.name('Long Position');
    // seriesLong.stroke('#008000');
    seriesLong.stroke("#008000", 2, "0");
    seriesLong.legendItem().iconType('rising-falling');

    var series3 = plot2.line(mapping02);
    series3.name('Line of Profit');
    // series3.labels(true);
    series3.legendItem().iconType('rising-falling');

    // mapping2
    // set settings for event markers
    var eventMarkers2 = plot2.eventMarkers();
    // set markers data
    // this.measuredDataDifference

    console.log("testing");
    // console.log(this.measuredDataDifference);
    eventMarkers2.data([
        { date: measuredDataDifference[0].date, description: 'Highest Closing Price, ' + measuredDataDifference[0].close },
        // { date: this.measuredDataDifference[0].date, description: 'Highest Closing Price, ' + this.measuredDataDifference[0].close + ', ' + (Math.round(this.measuredDataDifference[0].difference * 100) / 100) + "x of intial price" },
    ]);

    // create scroller series with mapped data
    chart2.scroller().area(mapping22);

    // remove previous childs
    let currentContainer2 = document.getElementById("container2");
    while (currentContainer2.firstChild) {
        currentContainer2.removeChild(currentContainer2.firstChild);
    }

    chart2.container('container2');
    // initiate chart drawing
    chart2.draw();

    // create range picker
    var rangePicker2 = anychart.ui.rangePicker();
    // init range picker
    rangePicker2.render(chart2);

    // create range selector
    var rangeSelector2 = anychart.ui.rangeSelector();
    // init range selector
    rangeSelector2.render(chart2);

    // Grapgh 2 ends here

    // remove previous childs
    let currentContainer = document.getElementById("container");
    while (currentContainer.firstChild) {
        currentContainer.removeChild(currentContainer.firstChild);
    }

    // set chart selected date/time range
    // chart.selectRange('2001-12-23', '2006-09-10');

    // set container id for the chart
    chart.container('container');
    // initiate chart drawing
    chart.draw();

    // create range picker
    var rangePicker = anychart.ui.rangePicker();
    // init range picker
    rangePicker.render(chart);

    // create range selector
    var rangeSelector = anychart.ui.rangeSelector();
    // init range selector
    rangeSelector.render(chart);
    // });

    // this.setState({ loadingStockData: false });
    // this.forceUpdate();
    // this.saveCurrentStateBackup();
}

export default loadChart;
