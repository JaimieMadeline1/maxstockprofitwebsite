self.addEventListener("message", (e) => {
    // the passed-in data is available via e.data
    console.log(e.data);
    // console.log(e.data);
    // console.log(e.data.calcChartData);
    const result = calcChartData(e.data);
    console.log(result);
    self.postMessage(result);
}, false);

const calcChartData = (chartDataParam) => {
    if (chartDataParam) {

        let { dataTemp, stockName, companyName, typeOfInterval,
            inputIntervalNumber, avoidVolalityNumber, avoidVolalityDaysNearby } = chartDataParam;

        // let close = dataTemp[0].close;
        // dataTemp.reverse();
        // console.log(dataTemp);
        // console.log(close);
        // this.setState(prevState => {
        //     return { anyStockChartData: dataTemp, tickerList: [...prevState.tickerList, { stockName: stockName, companyName: companyName, riskRating: 80 }] }
        // });
        const criticalPointConst = 0.005;
        let timePerTrade = 30 * 4;
        // if (this.state.typeOfInterval == 'Weekly') {
        //     timePerTrade = this.state.inputIntervalNumber;
        // } else 
        // console.log(timePerTrade);
        if (typeOfInterval == 'Month') {
            timePerTrade = Math.round(30 / inputIntervalNumber);
        } else if (typeOfInterval == 'Year') {
            timePerTrade = Math.round(252 / inputIntervalNumber);
        }
        // console.log(timePerTrade);
        // Yearly
        const timerPerTradeImportance = 2; // bigger value, means less importance on timing iteration accuracy
        const heightToGetReturnImportance = 2;
        let avoidingVolatilityImportance = avoidVolalityNumber;
        const avoidVolatilityNumberOfStocksAtSide = avoidVolalityDaysNearby;
        const avoidVolatilityDaysApart = 3;

        console.log("testing data temp.")
        console.log(dataTemp);

        const differenceDataTemp = dataTemp;
        // for (let i = 1; i < dataTemp.length; i++) {
        //     differenceDataTemp.push({ date: dataTemp[i - 1].date, close : (dataTemp[i].close - dataTemp[i - 1].close)});
        // }
        // console.log(differenceDataTemp);

        const criticalPointsDataTemp = [];
        for (let i = 0; i < differenceDataTemp.length - 1; i++) {
            // console.log(differenceDataTemp[i].close * (1 - criticalPointConst));
            // console.log(differenceDataTemp[i + 1].close);
            // console.log((differenceDataTemp[i].close * (1 + criticalPointConst)));
            if (((differenceDataTemp[i].close * (1 - criticalPointConst)) > differenceDataTemp[i + 1].close) || (differenceDataTemp[i + 1].close > (differenceDataTemp[i].close * (1 + criticalPointConst)))) {
                criticalPointsDataTemp.push({ date: differenceDataTemp[i].date, value: differenceDataTemp[i].close });
            }
        }
        // console.log(criticalPointsDataTemp);

        const measuredDataDifference = [];
        for (let i = criticalPointsDataTemp.length - 1; i >= 0; i--) {
            let temp = (criticalPointsDataTemp[i].value - criticalPointsDataTemp[criticalPointsDataTemp.length - 1].value);
            // console.log(criticalPointsDataTemp[criticalPointsDataTemp.length].value);
            // console.log(criticalPointsDataTemp[i].value);
            // console.log(temp);
            // if (temp < 0) {
            //     temp = temp * (-1);
            // }
            let differencePercent = (temp) / criticalPointsDataTemp[criticalPointsDataTemp.length - 1].value;
            measuredDataDifference.push({ difference: differencePercent, date: criticalPointsDataTemp[i].date, close: criticalPointsDataTemp[i].value });
        }
        // const measuredDataTemp = criticalPointsDataTemp;

        const finalMeasuedData = [];
        let measuredData = [];
        let startNumber = criticalPointsDataTemp.length - 1;
        let isContinue = true;
        const getAverage = (elmts) => {
            let sum = 0;
            for (let i = 0; i < elmts.length; i++) {
                // sum += parseInt(elmts[i], 10); //don't forget to add the base
                sum += elmts[i];
            }

            const avg = sum / elmts.length;
            return avg;
        }
        const calculateAvoidVolatlityRatingHelper = (list, q, curr) => {
            let obj1 = list[q];
            let objCurr = list[curr];
            // console.log(list);
            // console.log(q);
            // console.log(curr);
            let diff = list[q].value - list[curr].value;
            if (diff < 0) {
                diff = diff * (-1);
            }
            // Date calculations; The farther the less importance
            let currItemDateObjCompare = new Date(objCurr.date);
            let qItemDateObjCompare = new Date(obj1.date);
            let daysApart = (currItemDateObjCompare.getTime() - qItemDateObjCompare.getTime()) / (24 * 60 * 60 * 1000);
            if (daysApart < 0) {
                daysApart = daysApart * (-1);
            }
            // console.log('diff');
            // console.log(diff);
            // console.log(daysApart);
            // console.log(avoidVolatilityDaysApart);
            let finalizedAns = diff * ((avoidVolatilityDaysApart) / (daysApart + (avoidVolatilityDaysApart / 2)));
            return finalizedAns;
        }
        const calculateAvoidVolatlityRating = (dataList, curr) => {
            // if (curr < 3 || curr > criticalPointsDataTemp.length - 4) {
            //     return 1;
            // } else {
            let valuesList = [];
            let counterDown = 0;
            let counterUp = 0;
            let finalAns = 0;
            for (let q = curr + 1; (q < dataList.length) && (counterUp < avoidVolatilityNumberOfStocksAtSide); q++) {
                valuesList.push(calculateAvoidVolatlityRatingHelper(dataList, q, curr));
                counterUp++;
            }
            for (let w = curr - 1; (w >= 0) && (counterDown < avoidVolatilityNumberOfStocksAtSide); w--) {
                valuesList.push(calculateAvoidVolatlityRatingHelper(dataList, w, curr));
                counterDown++;
            }
            if (valuesList.length < avoidVolatilityNumberOfStocksAtSide * 2) {
                const missing = (avoidVolatilityNumberOfStocksAtSide * 2) - valuesList.length;
                // let average = _.meanBy(valuesList);
                let average = getAverage(valuesList);
                // console.log(missing);
                // console.log(average);
                // console.log(valuesList);
                // console.log(dataList[curr].date);
                finalAns = average * missing;
            }
            // console.log(valuesList);
            for (let t = 0; t < valuesList.length; t++) {
                finalAns += valuesList[t];
            }
            // console.log(valuesList);
            // console.log(finalAns);
            // }
            return finalAns;
        }
        // console.log(avoidingVolatilityImportance);
        while (isContinue == true) {
            measuredData = [];
            for (let i = startNumber; i >= 0; i--) {
                let temp = (criticalPointsDataTemp[i].value - criticalPointsDataTemp[startNumber].value);
                if (temp < 0) {
                    temp = temp * (-1);
                }
                let differencePercent = (temp) / criticalPointsDataTemp[startNumber].value;
                // compare the dates, take way
                // console.log(new Date(criticalPointsDataTemp[criticalPointsDataTemp.length - 1].date).toDateString());
                let startItemDateObj = new Date(new Date(criticalPointsDataTemp[startNumber].date).getTime() + (timePerTrade * 24 * 60 * 60 * 1000));
                // console.log(startItemDateObj.toDateString());
                let currItemDateObj = new Date(criticalPointsDataTemp[i].date);

                let dateModifier = (currItemDateObj.getTime() - startItemDateObj.getTime()) / (24 * 60 * 60 * 1000);
                if (dateModifier < 0) {
                    dateModifier = dateModifier * (-1);
                }

                let returnImportance = differencePercent - (differencePercent * dateModifier / (timePerTrade * timerPerTradeImportance));
                let avoidVolatilityImportance = calculateAvoidVolatlityRating(criticalPointsDataTemp, i);
                // i = 0;
                // isContinue = false;
                // console.log(avoidVolatilityImportance);
                // console.log(avoidingVolatilityImportance);
                // console.log((avoidVolatilityImportance * avoidingVolatilityImportance));
                let importance = (returnImportance * heightToGetReturnImportance) - ((avoidVolatilityImportance / 10) * avoidingVolatilityImportance);
                // let importance = (returnImportance * heightToGetReturnImportance);

                measuredData.push({
                    dateModifier: dateModifier, importance: importance,
                    difference: differencePercent, date: criticalPointsDataTemp[i].date, close: parseFloat(criticalPointsDataTemp[i].value), numberInArray: i
                });
            }
            const compare2 = (a, b) => {
                if (a.importance < b.importance)
                    return 1;
                if (a.importance > b.importance)
                    return -1;
                return 0;
            }
            measuredData.sort(compare2);
            // console.log(measuredData);

            startNumber = measuredData[0].numberInArray - 1;
            finalMeasuedData.push(measuredData[0]);

            let currItemDateObjCompare = new Date(measuredData[0].date);
            let lastItemDateObjCompare = new Date(criticalPointsDataTemp[0].date)

            let daysLeft = (lastItemDateObjCompare.getTime() - currItemDateObjCompare.getTime()) / (24 * 60 * 60 * 1000);
            if (daysLeft < timePerTrade) {
                isContinue = false;
            }

        }
        // if (false) {
        console.log(finalMeasuedData);

        // Calculate long and short profit
        let tempList = [];
        let longProfit = 0;
        let shortProfit = 0;
        let longProfitCompounded = 1.0;
        let shortProfitCompounded = 1.0;
        let lastPosition = 'none';
        let profit = 0;
        let daysApart = 0;
        let dateRange = '';
        for (let t = 1; t < finalMeasuedData.length; t++) {
            // date diff
            daysApart = 0;
            daysApart = getDaysApart(new Date(finalMeasuedData[t - 1].date), new Date(finalMeasuedData[t].date));

            // let temp1 = new Date(finalMeasuedData[t - 1].date);
            // let temp2 = new Date(finalMeasuedData[t].date);

            // temp1.getDate
            const dateFormating = (temp) => {
                // console.log(monthNames);
                // return temp.getFullYear() + '/' + monthNames[temp.getMonth()] + '/' + temp.getDay();
                // return temp.getFullYear() + '/' + temp.getMonth() + '/' + temp.getDate();
                return temp.replace(/\-/g, '/');
            }
            dateRange = dateFormating(finalMeasuedData[t - 1].date) + ' - ' + dateFormating(finalMeasuedData[t].date);

            // same
            if (finalMeasuedData[t - 1].close == finalMeasuedData[t].close) {
                // console.log("same");
                if (lastPosition == 'none') {
                    tempList.push({ obj: finalMeasuedData[t - 1], dateRange: dateRange, startClose: finalMeasuedData[t].close, position: 'long', positionExact: 'Long', isHold: false, profitPercent: 0 + "%", daysApart: daysApart });
                } else {
                    tempList.push({ obj: finalMeasuedData[t - 1], dateRange: dateRange, startClose: finalMeasuedData[t].close, position: lastPosition, positionExact: 'Hold', isHold: true, profitPercent: 0 + "%", daysApart: daysApart });
                }
            }
            // short position
            if (finalMeasuedData[t - 1].close > finalMeasuedData[t].close) {
                // console.log("short");
                profit = (finalMeasuedData[t - 1].close - finalMeasuedData[t].close) / finalMeasuedData[t - 1].close * 100;
                profit = Math.round(profit * 100) / 100;
                if (lastPosition == 'none' || lastPosition == 'long') {
                    tempList.push({ obj: finalMeasuedData[t - 1], dateRange: dateRange, startClose: finalMeasuedData[t].close, position: 'short', positionExact: 'Short', isHold: false, profitPercent: profit + "%", daysApart: daysApart });
                }
                if (lastPosition == 'short') {
                    tempList.push({ obj: finalMeasuedData[t - 1], dateRange: dateRange, startClose: finalMeasuedData[t].close, position: 'short', positionExact: 'Hold Short', isHold: true, profitPercent: profit + "%", daysApart: daysApart });
                }
                shortProfit += profit;
                let additionalPercent = (finalMeasuedData[t - 1].close - finalMeasuedData[t].close / finalMeasuedData[t - 1].close) / 100;
                shortProfitCompounded = shortProfitCompounded * (1 + additionalPercent);
                lastPosition = 'short';
            }
            // long position
            if (finalMeasuedData[t - 1].close < finalMeasuedData[t].close) {
                // console.log("long");
                profit = (finalMeasuedData[t].close - finalMeasuedData[t - 1].close) / finalMeasuedData[t - 1].close * 100;
                profit = Math.round(profit * 100) / 100;
                if (lastPosition == 'none' || lastPosition == 'short') {
                    tempList.push({ obj: finalMeasuedData[t - 1], dateRange: dateRange, startClose: finalMeasuedData[t].close, position: 'long', positionExact: 'Long', isHold: false, profitPercent: profit + "%", daysApart: daysApart });
                }
                else if (lastPosition == 'long') {
                    tempList.push({ obj: finalMeasuedData[t - 1], dateRange: dateRange, startClose: finalMeasuedData[t].close, position: 'long', positionExact: 'Hold Long', isHold: true, profitPercent: profit + "%", daysApart: daysApart });
                }
                longProfit += profit;
                let additionalPercent = (finalMeasuedData[t - 1].close - finalMeasuedData[t].close / finalMeasuedData[t - 1].close) / 100;
                longProfitCompounded = longProfitCompounded * (1 + additionalPercent);
                lastPosition = 'long';
            }
        }
        daysApart = 0;
        daysApart = getDaysApart(new Date(finalMeasuedData[finalMeasuedData.length - 2].date), new Date(finalMeasuedData[finalMeasuedData.length - 1].date));
        // Include first and last position
        if (tempList[tempList.length - 1].position == 'long') {
            tempList.push({ obj: finalMeasuedData[finalMeasuedData.length - 1], position: 'long', positionExact: 'Sell Long Position', isHold: true, daysApart: daysApart });
        } else {
            tempList.push({ obj: finalMeasuedData[finalMeasuedData.length - 1], position: 'short', positionExact: 'Buy Out Of Short Position', isHold: true, daysApart: daysApart });
        }

        // const measuredDataTemp = criticalPointsDataTemp;
        console.log("short: " + shortProfit);
        console.log("long: " + longProfit);

        console.log("short compounded: " + shortProfitCompounded);
        console.log("long compounded: " + longProfitCompounded);

        // order
        const compare = (a, b) => {
            if (a.difference < b.difference)
                return 1;
            if (a.difference > b.difference)
                return -1;
            return 0;
        }
        measuredDataDifference.sort(compare);

        console.log(measuredDataDifference);

        return { finalMeasuedData, tempList, measuredDataDifference, dataTemp, stockName, companyName };
    }
    return false;
}

const getDaysApart = (date1, date2) => {
    let msDiff = date2.getTime() - date1.getTime();
    msDiff = Math.ceil(msDiff / (24 * 60 * 60 * 1000));
    return msDiff;
}
